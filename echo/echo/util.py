def ellipsis(string, length=79, count=3, style='.'):
    if len(string) > length:
        string = '{string}{ellipsis}'.format(
            string   = string[:length],
            ellipsis = style * count
        )

    return string

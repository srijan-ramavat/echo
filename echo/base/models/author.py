from django.db                  import models
from django.contrib.auth.models import User

class Author(models.Model):
    user = models.OneToOneField(User,
        on_delete  = models.CASCADE,
        null       = True
    )
    name = models.CharField(
        max_length = 70,
        null       = True
    )

    def __unicode__(self):
        return '<Author {name}>'.format(
            name = self.user.get_full_name() if self.user else self.name
        )